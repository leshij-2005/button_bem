//
// import names
//

var gulp = require('gulp');
var gutil = require('gulp-util');

var del = require('del');

var connect = require('gulp-connect');

var postcss = require('gulp-postcss');
var prefixer = require('gulp-autoprefixer');
var pxtorem = require('postcss-pxtorem');

var imagemin = require('gulp-imagemin');
// var pngquant = require('imagemin-pngquant');

var cssmin = require('gulp-clean-css');
var sass = require('gulp-sass');

var rigger = require('gulp-rigger');

var minify = require('gulp-minify');

var rename = require('gulp-rename');

var path = {
  style: {
    css: './src/style/scss/*.*',
    fonts: './src/style/fonts/**/*.*',
    images: './src/style/images/**/*.*'
  },
  images: './src/images/**/*.*',
  html: './src/pages/**/*.*',
  js: './src/js/**/*.*',
  outDir: './public/'
};

//
// helper functions
//

function imageBuild(src, dest){
  return gulp.src(src)
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      // use: [pngquant()],
      interlaced: true
    }))
    .on('error', function (err) {
      gutil.log(err);
      this.emit('end');
    })
    .pipe(gulp.dest(dest))
    .pipe(connect.reload());
}

//
// clean
//

gulp.task('clean', function () {
  return del(path.outDir);
});

//
// css build
//
 
gulp.task('css:build', function () {
  var processors = [
    pxtorem({
      propWhiteList: [],
      mediaQuery: true,
      replace: true
    })
  ];

  return gulp.src(path.style.css)
    .pipe(sass())
    .pipe(prefixer())
    .pipe(postcss(processors))
    .pipe(gulp.dest(path.outDir + 'css/'))
    .pipe(cssmin())
    .pipe(rename(function(path){
      path.basename += '.min';
    }))
    .pipe(gulp.dest(path.outDir + 'css/'))
    .pipe(connect.reload())
    .on('error', function (err) {
      gutil.log(err);
      this.emit('end');
    });
});

//
// image build
//

gulp.task('image:build', function () {
  imageBuild(path.images, path.outDir + 'images/');

  return imageBuild(path.style.images, path.outDir + 'css/images/');
});

//
// html build
//

gulp.task('html:build', function () {
  return gulp.src(path.html)
    .pipe(rigger())
    .on('error', function (err) {
      gutil.log(err);
      this.emit('end');
    })
    .pipe(gulp.dest(path.outDir))
    .pipe(connect.reload());
});

//
// js build
//

gulp.task('js:build', function () {
  return gulp.src(path.js)
    .pipe(rigger())
    .pipe(minify({
      ext: {
        src: '.js',
        min:'.min.js',
      },
      ignoreFiles: ['*.min.js']
    }))
    .on('error', function (err) {
      gutil.log(err);
      this.emit('end');
    })
    .pipe(gulp.dest(path.outDir + 'js/'))
    .pipe(connect.reload());
});

//
// fonts build
//

gulp.task('fonts:build', function () {
  return gulp.src(path.style.fonts)    
    .pipe(gulp.dest(path.outDir + 'css/fonts/'))
    .pipe(connect.reload());
});

//
// server
//

gulp.task('webserver', function(){
  return connect.server({
    root: [path.outDir],
    port: 8003,
    livereload: true
  });
});

//
// build
//

gulp.task('build', gulp.parallel('html:build', 'css:build', 'image:build', 'js:build', 'fonts:build'));

//
// watch
//

gulp.task('watch', function() {
  gulp.watch('./src/style/scss/**/*.scss', gulp.series('css:build'));
  gulp.watch(path.style.fonts, gulp.series('fonts:build'));
  gulp.watch(path.images, gulp.series('image:build'));
  gulp.watch(path.style.images, gulp.series('image:build'));
  gulp.watch('./src/**/*.html', gulp.series('html:build'));
  gulp.watch(path.js, gulp.series('js:build'));
});

//
// default
//
 
gulp.task('default', gulp.parallel('build', 'webserver', 'watch'));